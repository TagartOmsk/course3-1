package ru.omsu.imit.lambda;

import org.junit.Test;
import ru.omsu.imit.lambda.person.Person;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

public class LambdasTest {

    @Test
    public void testSplitCount() {
        String str = "Applaus und Klatschen sowie Lachen begleiten die Pause.";
        assertEquals(8, (int) Lambdas.count.apply(Lambdas.split.apply(str)));
        assertEquals(8, (int) Lambdas.splitAndCountAndThen.apply(str));
        assertEquals(8, (int) Lambdas.splitAndCountComposed.apply(str));
    }

    @Test
    public void testCreate() {
        String str = "Michael";
        Person man = new Person(str);
        assertEquals(man, Lambdas.create.apply(str));
    }

    @Test
    public void testPredicates() {
        Random rand = new Random();
        int r = rand.nextInt(100);
        assertEquals(r % 2 == 0, Lambdas.isEven.test(r));
        assertFalse(Lambdas.areEqual.test(-1, r));
    }

    @Test
    public void testSumProduct() {
        List<Integer> list = Arrays.asList(1,2,3,4,5);

        assertEquals(15, Lambdas.sum(list));

        assertEquals(120, Lambdas.product(list));
    }
}