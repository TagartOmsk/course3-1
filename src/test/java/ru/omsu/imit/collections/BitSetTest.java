package ru.omsu.imit.collections;

import org.junit.Test;

import java.util.BitSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static ru.omsu.imit.collections.BitSetProcessor.addInts;
import static ru.omsu.imit.collections.BitSetProcessor.find;
import static ru.omsu.imit.collections.BitSetProcessor.remove;

public class BitSetTest {

    @Test
    public void testBitSet() {
        int[] arr = {0, 1, 1, 2, 3, 5, 8, 13, 21};
        BitSet bitSet = new BitSet(),
        bitSet1 = new BitSet();

        addInts(bitSet, arr);
        addInts(bitSet1, arr);
        addInts(bitSet1, 34);

        assertTrue(find(bitSet1, 34));
        assertFalse(find(bitSet, 34));

        remove(bitSet1, 34);

        assertEquals(bitSet, bitSet1);
    }
}
