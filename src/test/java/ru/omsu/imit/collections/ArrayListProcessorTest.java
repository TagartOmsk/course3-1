package ru.omsu.imit.collections;

import org.junit.Test;
import ru.omsu.imit.ttschool.Trainee;
import ru.omsu.imit.ttschool.TrainingException;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static ru.omsu.imit.collections.ArrayListProcessor.*;

public class ArrayListProcessorTest {

    @Test
    public void testReverse() throws TrainingException {
        ArrayList<Trainee> expected = new ArrayList<>(), actual = new ArrayList<>();
        Trainee ronald = new Trainee("Ronald", "McDonald", 4),
                lovecraft = new Trainee("Howard", "Lovecraft", 3),
                beethoven = new Trainee("Ludvig", "van Beethoven", 4);
        expected.add(ronald);
        expected.add(lovecraft);
        expected.add(beethoven);

        actual.add(beethoven);
        actual.add(lovecraft);
        actual.add(ronald);

        assertArrayEquals(expected.toArray(), reverse(actual).toArray());
    }

    @Test
    public void testMoveByTwo() throws TrainingException {
        ArrayList<Trainee> expected = new ArrayList<>(), actual = new ArrayList<>();
        Trainee ronald = new Trainee("Ronald", "McDonald", 4),
                lovecraft = new Trainee("Howard", "Lovecraft", 3),
                beethoven = new Trainee("Ludvig", "van Beethoven", 4);

        actual.add(ronald);
        actual.add(lovecraft);
        actual.add(beethoven);

        expected.add(lovecraft);
        expected.add(beethoven);
        expected.add(ronald);

        assertArrayEquals(expected.toArray(), moveByTwo(actual).toArray());
    }

    @Test
    public void testShuffle() throws TrainingException {
        ArrayList<Trainee> unexpected = new ArrayList<>();

        Trainee ronald = new Trainee("Ronald", "McDonald", 4),
                lovecraft = new Trainee("Howard", "Lovecraft", 3),
                beethoven = new Trainee("Ludvig", "van Beethoven", 4);

        unexpected.add(lovecraft);
        unexpected.add(beethoven);
        unexpected.add(ronald);

        // sometimes can fail as it's random
        assertFalse(unexpected.equals(shuffle(unexpected)));
    }

    @Test
    public void testGetMaxRate() throws TrainingException {
        ArrayList<Trainee> actual = new ArrayList<>(), expected = new ArrayList<>();
        Trainee ronald = new Trainee("Ronald", "McDonald", 4),
                lovecraft = new Trainee("Howard", "Lovecraft", 3),
                beethoven = new Trainee("Ludvig", "van Beethoven", 4);

        actual.add(ronald);
        actual.add(lovecraft);
        actual.add(beethoven);

        expected.add(ronald);
        expected.add(beethoven);

        assertTrue(getMaxRate(actual).contains(ronald));
        assertTrue(getMaxRate(actual).contains(beethoven));
        assertFalse(getMaxRate(actual).contains(lovecraft));
    }

    @Test
    public void testSortByRate() throws TrainingException {
        ArrayList<Trainee> expected = new ArrayList<>(), actual = new ArrayList<>();
        Trainee ronald = new Trainee("Ronald", "McDonald", 4),
                lovecraft = new Trainee("Howard", "Lovecraft", 3),
                beethoven = new Trainee("Ludvig", "van Beethoven", 5);

        actual.add(ronald);
        actual.add(lovecraft);
        actual.add(beethoven);

        expected.add(beethoven);
        expected.add(ronald);
        expected.add(lovecraft);

        assertArrayEquals(expected.toArray(), sortByRate(actual).toArray());
    }

    @Test
    public void testSortByName() throws TrainingException {
        ArrayList<Trainee> expected = new ArrayList<>(), actual = new ArrayList<>();
        Trainee ronald = new Trainee("Ronald", "McDonald", 4),
                lovecraft = new Trainee("Howard", "Lovecraft", 3),
                beethoven = new Trainee("Ludvig", "van Beethoven", 5);

        actual.add(ronald);
        actual.add(lovecraft);
        actual.add(beethoven);

        expected.add(lovecraft);
        expected.add(beethoven);
        expected.add(ronald);

        assertArrayEquals(expected.toArray(), sortByName(actual).toArray());
    }

    @Test
    public void testFindByName() throws TrainingException {
        ArrayList<Trainee> expected = new ArrayList<>(), actual = new ArrayList<>();
        Trainee ronald = new Trainee("Ronald", "McDonald", 4),
                lovecraft = new Trainee("Howard", "Lovecraft", 3),
                beethoven = new Trainee("Ludvig", "van Beethoven", 5);

        actual.add(ronald);
        actual.add(lovecraft);
        actual.add(beethoven);

        assertEquals(ronald, findByName(actual, "Ronald McDonald"));

        assertNull(findByName(actual, "SRGdb"));

    }
}