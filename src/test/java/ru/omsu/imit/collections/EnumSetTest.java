package ru.omsu.imit.collections;

import org.junit.Test;
import ru.omsu.imit.colors.Color;

import java.util.EnumSet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EnumSetTest {
    EnumSet<Color> all = EnumSet.allOf(Color.class),
            one = EnumSet.of(Color.RED),
            interval = EnumSet.range(Color.RED, Color.GREEN),
            none = EnumSet.noneOf(Color.class);

    @Test
    public void testFindColors() {
        assertTrue(all.contains(Color.RED));
        assertFalse(one.contains(Color.GREEN));
        // RED < GREEN < BLUE
        assertFalse(interval.contains(Color.BLUE));
        assertFalse(none.contains(Color.RED));
    }
}
