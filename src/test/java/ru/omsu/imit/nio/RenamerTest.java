package ru.omsu.imit.nio;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;

public class RenamerTest {

    @Before
    public void start() throws IOException {
        Files.deleteIfExists(Paths.get("src/main/files/directory/file.bin"));
        Files.deleteIfExists(Paths.get("src/main/files/directory/sas.bin"));
        Files.deleteIfExists(Paths.get("src/main/files/directory/none.bin"));
        Files.createFile(Paths.get("src/main/files/directory/none.dat"));
        Files.createFile(Paths.get("src/main/files/directory/sas.dat"));
        Files.createFile(Paths.get("src/main/files/directory/file.dat"));
    }

    @Test
    public void testRename() throws IOException {
        Path dir = Paths.get("src/main/files/directory");
        assertTrue(Files.exists(Paths.get("src/main/files/directory/file.dat")));
        assertTrue(Files.exists(Paths.get("src/main/files/directory/none.dat")));
        assertTrue(Files.exists(Paths.get("src/main/files/directory/sas.dat")));
        Renamer.renameDatToBin(dir);
        assertTrue(Files.exists(Paths.get("src/main/files/directory/file.bin")));
        assertTrue(Files.exists(Paths.get("src/main/files/directory/none.bin")));
        assertTrue(Files.exists(Paths.get("src/main/files/directory/sas.bin")));
    }
}