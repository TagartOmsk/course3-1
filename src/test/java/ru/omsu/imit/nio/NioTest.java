package ru.omsu.imit.nio;

import org.junit.Test;
import ru.omsu.imit.ttschool.Trainee;
import ru.omsu.imit.ttschool.TrainingException;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class NioTest {
    @Test
    public void testByteBuffer() throws IOException, TrainingException {
        Path path = Paths.get("src/main/files/trainee");
        File file = new File(path.toUri());
        if (!file.exists()) {
            file.createNewFile();
        }
        Trainee expected = new Trainee("Фалес", "Милетский", 5);

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            writer.write(expected.getFullName() + " ");
            writer.write(Integer.toString(expected.getRating()));
        }

        assertEquals(expected, NIOProcessor.readTrainee(path));
    }

    @Test
    public void testMappedByteBuffer() throws IOException, TrainingException {
        Path path = Paths.get("src/main/files/trainee");
        File file = new File(path.toUri());
        if (!file.exists()) {
            file.createNewFile();
        }
        Trainee expected = new Trainee("Фалес", "Милетский", 5);

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            writer.write(expected.getFullName() + " ");
            writer.write(Integer.toString(expected.getRating()));
        }

        assertEquals(expected, NIOProcessor.readTraineeMapped(Paths.get("src/main/files/trainee")));
    }

    @Test
    public void testMappedWriteRead() throws IOException {

        Path path = Paths.get("src/main/files/nums");
        File file = new File(path.toUri());
        file.createNewFile();

        try (RandomAccessFile stream = new RandomAccessFile(file, "rw")) {

            NIOProcessor.writeHundredNumbersMapped(path);

            for (int i = 0; i < 100; i++) {
                assertEquals(i, stream.readInt());
            }
        }
    }

    @Test
    public void testSerializeByteBuffer() throws TrainingException, IOException, ClassNotFoundException {
        Trainee tr = new Trainee("Nate", "Higgers", 3);

        ByteBuffer buffer = NIOProcessor.serializeTraineeToByteBuffer(tr, 100);


        Trainee actual;
        try (ObjectInputStream stream = new ObjectInputStream(new ByteArrayInputStream(buffer.array()))) {
            actual = (Trainee) stream.readObject();
        }

        assertEquals(tr, actual);
    }
}
