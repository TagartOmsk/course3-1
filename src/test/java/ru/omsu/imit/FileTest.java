package ru.omsu.imit;

import org.junit.Test;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.*;

public class FileTest {
    @Test
    public void testCreateDeleteRename() {
        File file = new File("./ultra_space_troll"),
                nestedFile = new File(file, "ohMyyyy"),
                renamed = new File(file, "renamed");
        assertFalse(file.exists());
        assertFalse(nestedFile.exists());
        try {
            file.createNewFile();
        } catch (IOException exc) {
            exc.printStackTrace();
            fail();
        }
        assertTrue(file.exists());
        assertFalse(file.isDirectory());

        file.delete();
        assertFalse(file.exists());

        try {
            file.mkdir();
            nestedFile.createNewFile();
            assertTrue(nestedFile.exists());
            assertEquals("ohMyyyy", nestedFile.getName());
            nestedFile.renameTo(renamed);

            File temp = file.listFiles()[0];
            assertEquals(renamed.getName(), temp.getName());
            nestedFile.delete();
            renamed.delete();
            assertFalse(nestedFile.exists());
            file.delete();
            assertFalse(file.exists());
        } catch (IOException exc) {
            exc.printStackTrace();
            fail();
        }
    }

    @Test
    public void testFullName() {
        File dir = new File("/home/vyacheslav/Projects"), file = new File(dir, "check");
        assertTrue(dir.exists());
        assertTrue(dir.isDirectory());
        try {
            file.createNewFile();
        } catch (IOException exc) {
            exc.printStackTrace();
            fail();
        }
        assertEquals(dir.getPath() + "/" + file.getName(), file.getPath());
    }

    @Test
    public void testExists() {
        File folder1 = new File("/home/vyacheslav/Projects");
        File folder2 = new File("/home/vyacheslav/ЫЩ");
        File file1 = new File("./pom.xml");
        assertTrue(folder1.exists());
        if (folder1.exists()) {
            assertTrue(folder1.isDirectory());
        }
        assertFalse(folder2.exists());
        if (folder2.exists()) {
            assertTrue(folder2.isDirectory());
        }
        assertTrue(file1.exists());
        if (file1.exists()) {
            assertTrue(file1.isFile());
        }
    }

    @Test
    public void testList() {
        File root = new File("/home/vyacheslav/IdeaProjects/course3-1/");
        File[] arr = root.listFiles();
        File file = new File("/home/vyacheslav/IdeaProjects/course3-1");
        File[] arr1 = file.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.contains(".xml");
            }
        });
        assertEquals(7, arr.length);
        assertEquals(1, arr1.length);

    }
}
