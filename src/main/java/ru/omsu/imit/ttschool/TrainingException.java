package ru.omsu.imit.ttschool;

public class TrainingException extends Exception {
    private TrainingErrorCode code;

    public TrainingException(TrainingErrorCode code) {
        super(code.getErrorString());
        this.code = code;
    }

    public TrainingErrorCode getErrorCode() {
        return code;
    }
}
