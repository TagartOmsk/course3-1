package ru.omsu.imit.ttschool;

import java.util.*;

public class Group {
    private String groupName, roomName;
    private List<Trainee> students;

    public Group(String name, String room) throws TrainingException {
        students = new ArrayList<>();
        setName(name);
        setRoom(room);
    }

    public String getName() {
        return groupName;
    }

    public void setName(String name) throws TrainingException {
        if (name == null || name.equals("")) throw new TrainingException(TrainingErrorCode.GROUP_WRONG_NAME);
        groupName = name;
    }


    public String getRoom() {
        return roomName;
    }

    public void setRoom(String room) throws TrainingException {
        if (room == null || room.equals("")) throw new TrainingException(TrainingErrorCode.GROUP_WRONG_ROOM);
        roomName = room;
    }

    public List<Trainee> getTrainees() {
        return students;
    }

    public void addTrainee(Trainee trainee) {
        students.add(trainee);
    }

    public void removeTrainee(Trainee trainee) throws TrainingException {
        if (!students.remove(trainee)) throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }

    public void removeTrainee(int index) throws TrainingException {
        try {
            students.remove(index);
        } catch (IndexOutOfBoundsException e) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
    }

    public Trainee getTraineeByFirstName(String firstName) throws TrainingException {
        for (Trainee trn : students) {
            if (trn.getFirstName().equals(firstName)) return trn;
        }
        throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }

    public Trainee getTraineeByFullName(String fullName) throws TrainingException {
        for (Trainee trn : students) {
            if (trn.getFullName().equals(fullName)) return trn;
        }
        throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }

    public void sortTraineeListByFirstNameAscendant() {
        Collections.sort(students, Comparator.comparing(Trainee::getFirstName));
    }

    public void sortTraineeListByRatingDescendant() {
        Collections.sort(students, (s, t) -> t.getRating() - s.getRating());
    }

    public void reverseTraineeList() {
        Collections.reverse(students);
    }

    public void rotateTraineeList(int positions) {
        Collections.rotate(students, positions);
    }

    public List<Trainee> getTraineesWithMaxRating() throws TrainingException {
        if (students.isEmpty()) throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        List<Trainee> res = new ArrayList<>();
        int max = 0;
        for (Trainee trainee : students) {
            if (trainee.getRating() > max) {
                max = trainee.getRating();
                res.clear();
                res.add(trainee);
            } else {
                if (trainee.getRating() == max) res.add(trainee);
            }
        }
        return res;
    }

    public boolean hasDuplicates() {
        return new HashSet<>(students).size() < students.size();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(groupName, group.groupName) &&
                Objects.equals(roomName, group.roomName) &&
                Objects.equals(students, group.students);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupName, roomName, students);
    }
}
