package ru.omsu.imit.collections;

import java.util.*;

public class ListSpeedTest {

    public static long fillGivenList(List<Integer> list) {
        list.clear();
        long start = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            list.add(i);
        }
        long finish = System.nanoTime();
        return finish - start;
    }

    public static long randomAccessToList(List<Integer> list) {
        Random rand = new Random();
        long start = System.nanoTime();

        for (int i = 0; i < 100000; i++) {
            list.get(rand.nextInt(list.size()));
        }

        long finish = System.nanoTime();
        return finish - start;
    }

    public static long fillRandomly(Collection<Integer> col) {
        Random rand = new Random();
        col.clear();
        long start = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            col.add(rand.nextInt(1000000));
        }
        long finish = System.nanoTime();
        return finish - start;
    }

    public static long randomSearch(Collection<Integer> col) {
        Random rand = new Random();
        long start = System.nanoTime();
        for(int i = 0; i < 10000; i++) {
            col.contains(rand.nextInt(1000000));
        }
        long finish = System.nanoTime();
        return finish - start;
    }

    public static void main(String args[]) {
        List<Integer> array = new ArrayList<>(),
                linked = new LinkedList<>();
        //10
        long fill = fillGivenList(array);
        System.out.printf("ArrayList filling speed: %d ns\n", fill);
        long acc = randomAccessToList(array);
        System.out.printf("ArrayList access speed: %d ns\n", acc);
        System.out.printf("Total ArrayList speed: %d ns\n\n", fill + acc);


        fill = fillGivenList(linked);
        System.out.printf("LinkedList filling speed: %d ns\n", fill);
        acc = randomAccessToList(linked);
        System.out.printf("LinkedList access speed: %d ns\n", acc);
        System.out.printf("Total LinkedList speed: %d ns\n\n", fill + acc);

        // 15
        fill = fillRandomly(array);
        System.out.printf("ArrayList random filling speed: %d ns\n", fill);
        acc = randomSearch(array);
        System.out.printf("ArrayList random number search speed: %d ns\n", acc);
        System.out.printf("Total ArrayList speed: %d ns\n\n", fill + acc);

        Set<Integer> hash = new HashSet<>(),
                tree = new TreeSet<>();

        fill = fillRandomly(hash);
        System.out.printf("HashSet random filling speed: %d ns\n", fill);
        acc = randomSearch(hash);
        System.out.printf("HashSet random number search speed: %d ns\n", acc);
        System.out.printf("Total HashSet speed: %d ns\n\n", fill + acc);

        fill = fillRandomly(tree);
        System.out.printf("TreeSet random filling speed: %d ns\n", fill);
        acc = randomSearch(tree);
        System.out.printf("TreeSet random number search speed: %d ns\n", acc);
        System.out.printf("Total TreeSet speed: %d ns\n\n", fill + acc);
    }
}
