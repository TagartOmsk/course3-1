package ru.omsu.imit.collections;

import ru.omsu.imit.ttschool.Trainee;

import java.util.*;
import java.util.stream.Collectors;

public class ArrayListProcessor {

    public static List<Trainee> reverse(List<Trainee> list) {
        List<Trainee> res = new ArrayList<>(list);
        Collections.reverse(res);
        return res;
    }

    public static List<Trainee> moveByTwo(List<Trainee> list) {
        List<Trainee> res = new ArrayList<>(list);
        Trainee buf = null;
        for (int i = 0; i < res.size(); i++) {
            res.set((i + 2) % res.size(), list.get(i));
        }
        return res;
    }

    public static List<Trainee> shuffle(List<Trainee> list) {
        Random rand = new Random();
        List<Trainee> copy = new ArrayList<>(list), res = new ArrayList<>(list.size());
        int randNum = 0;
        while (!copy.isEmpty()) {
            randNum = rand.nextInt(copy.size());
            res.add(copy.get(randNum));
            copy.remove(randNum);
        }
        return res;
    }

    public static List<Trainee> getMaxRate(List<Trainee> list) {
        int max = 1;
        for (Trainee tr : list) {
            if (max < tr.getRating()) {
                max = tr.getRating();
            }
        }

        // lambda says variables must be final or effectively final
        final int MAX = max;

        return list.stream().filter(x -> x.getRating() == MAX).collect(Collectors.toList());
    }

    public static List<Trainee> sortByRate(List<Trainee> list) {
        return list.stream().sorted(new Comparator<Trainee>() {
            @Override
            public int compare(Trainee o1, Trainee o2) {
                return o2.getRating() - o1.getRating();
            }
        }).collect(Collectors.toList());
    }

    public static List<Trainee> sortByName(List<Trainee> list) {
        return list.stream().sorted(new Comparator<Trainee>() {
            @Override
            public int compare(Trainee o1, Trainee o2) {
                return o1.getFullName().compareToIgnoreCase(o2.getFullName());
            }
        }).collect(Collectors.toList());
    }

    public static Trainee findByName(List<Trainee> list, String name) {
        for (Trainee tr : list) {
            if (name.equals(tr.getFullName())) {
                return tr;
            }
        }
        return null;
    }
}

