package ru.omsu.imit.collections;

import java.util.BitSet;

public class BitSetProcessor {
    public static void addInts(BitSet set, int... ints) {
        for (int num : ints) {
            set.set(num);
        }
    }

    public static boolean find(BitSet set, int num) {
        return set.get(num);
    }

    public static void remove(BitSet set, int num) {
        set.clear(num);
    }
}
