package ru.omsu.imit.collections;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetSpeedTest {

    public static long fillSet(Set<Integer> set) {
        set.clear();
        long start = System.nanoTime();
        for(int i = 0; i <= 1000000; i++){
            set.add(i);
        }
        long finish = System.nanoTime();
        return finish - start;
    }

    public static long fillSet(BitSet set) {
        set.clear();
        long start = System.nanoTime();
        for(int i = 0; i <= 1000000; i++){
            set.set(i);
        }
        long finish = System.nanoTime();
        return finish - start;
    }

    public static void main(String[] args) {
        BitSet bitSet = new BitSet();
        HashSet<Integer> hash = new HashSet<>();
        TreeSet<Integer> tree = new TreeSet<>();
        System.out.printf("BitSet filling time: %d\n", fillSet(bitSet));
        System.out.printf("HashSet filling time: %d\n", fillSet(hash));
        System.out.printf("TreeSet filling time: %d\n", fillSet(tree));
    }
}
