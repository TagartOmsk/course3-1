package ru.omsu.imit.lambda.person_with_parents;

public class Person {
    private Person mother;
    private Person father;

    public Person getMother() {
        return mother;
    }

    public Person getFather() {
        return father;
    }

    public Person(Person mother, Person father) {
        this.mother = mother;
        this.father = father;
    }

    /*public Person getMothersMotherFather(){
        try{
            return getMother().getMother().getFather();
        }catch(NullPointerException e){
            return null;
        }
    }а так красиво бы было*/
    public Person getMothersMotherFather() {
        Person res = getMother();
        if (res == null) return res;
        res = res.getMother();
        if (res == null) return res;
        return res.getFather();
    }
}
