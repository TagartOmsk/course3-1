package ru.omsu.imit.lambda;

import java.util.function.Function;

@FunctionalInterface
public interface MyFunction<T, K> extends Function<T,K> {
    K apply(T arg);

    // K apply(T arg1, T arg2);
    // не компилируется. говорит, что нельзя больше одного метода в функциональном интерфейсе иметь
}
