package ru.omsu.imit.lambda;


import ru.omsu.imit.lambda.person.*;
import ru.omsu.imit.lambda.person_with_age.*;
import ru.omsu.imit.lambda.person_with_age.Person;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Lambdas {
    public static MyFunction<String, List<String>> split = s -> Arrays.asList(s.split(" "));
    public static Function<List<?>, Integer> count = List::size;
    public static MyFunction<String, Integer> splitAndCountAndThen = s -> split.andThen(count).apply(s);
    public static MyFunction<String, Integer> splitAndCountComposed = s -> count.compose(split).apply(s);

    public static MyFunction<String, ru.omsu.imit.lambda.person.Person> create = ru.omsu.imit.lambda.person.Person::new;

    public static BinaryOperator<Integer> maxInt = Math::max;
    public static BinaryOperator<Long> maxLong = Math::max;
    public static BinaryOperator<Float> maxFloat = Math::max;
    public static BinaryOperator<Double> maxDouble = Math::max;

    public static Supplier<Date> getCurrentDate = java.util.Date::new;

    public static Predicate<Integer> isEven = (i) -> i % 2 == 0;

    public static BiPredicate<Integer, Integer> areEqual = Integer::equals;

    public static IntStream transform(IntStream stream, IntUnaryOperator op) {
        return stream.map(op);
    }

    public static IntStream transformParallel(IntStream stream, IntUnaryOperator op) {
        return stream.parallel().map(op);
    }

    public static List<String> uniqueNamesAboveThirty(List<ru.omsu.imit.lambda.person_with_age.Person> list) {
        return list.stream()
                .filter(p -> p.getAge() > 30)
                .map(p -> p.getName())
                .sorted(String::compareTo)
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<String> uniqueNamesAboveThirtyGrouped(List<ru.omsu.imit.lambda.person_with_age.Person> list) {
        return list.stream()
                .collect(Collectors.groupingBy(ru.omsu.imit.lambda.person_with_age.Person::getName))
                .entrySet()
                .stream()
                .sorted(Comparator.comparingInt(a -> a.getKey().length()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public static int sum(List<Integer> list) {
        return list.stream().reduce((a, u) -> a + u).get();
    }

    public static int product(List<Integer> list) {
        return list.stream().reduce((a, u) -> a * u).get();
    }

    public static void main(String[] args) {
        List<ru.omsu.imit.lambda.person_with_age.Person> list = new ArrayList<>();
        list.add(new Person("Johnny", 40));
        list.add(new Person("Joseph", 32));
        list.add(new Person("Johnathan", 38));
        list.add(new Person("John", 55));

        Supplier<IntStream> newStream = () -> list.stream().mapToInt(Person::getAge);

        IntStream iStr = newStream.get(), copyStr = newStream.get();

        Lambdas.transformParallel(iStr, p -> 2*p).forEach(System.out::println);

        System.out.println();

        Lambdas.transform(copyStr, p -> 2*p).forEach(System.out::println);
    }
}
