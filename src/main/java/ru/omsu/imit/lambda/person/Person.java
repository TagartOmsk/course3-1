package ru.omsu.imit.lambda.person;

import java.util.Objects;

public class Person {
    private String name;

    public Person(String name) {
        if (name.isEmpty()) throw new RuntimeException("Empty name");
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.isEmpty()) throw new RuntimeException("Empty name");
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(getName(), person.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
