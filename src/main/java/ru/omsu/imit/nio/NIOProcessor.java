package ru.omsu.imit.nio;

import ru.omsu.imit.ttschool.Trainee;
import ru.omsu.imit.ttschool.TrainingException;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;

public class NIOProcessor {

    public static Trainee readTrainee(Path file) throws IOException, TrainingException {

        Trainee result;

        try (FileChannel channel = new FileInputStream(new File(file.toUri())).getChannel()) {
            ByteBuffer buffer = ByteBuffer.allocate((int) channel.size());
            channel.read(buffer);
            buffer.flip();
            String[] info = Charset.forName("UTF-8").decode(buffer).toString().split(" ");
            result = new Trainee(info[0], info[1], Integer.parseInt(info[2]));
        }
        return result;
    }

    public static Trainee readTraineeMapped(Path file) throws IOException, TrainingException {

        Trainee result;

        try (FileChannel channel = new FileInputStream(new File(file.toUri())).getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
            String[] data = Charset.forName("UTF-8").decode(buffer).toString().split(" ");
            result = new Trainee(data[0], data[1], Integer.parseInt(data[2]));
        }
        return result;
    }

    public static void writeHundredNumbersMapped(Path file) throws IOException {
        try (FileChannel channel = new RandomAccessFile(new File(file.toUri()), "rw").getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, 400);
            for (int i = 0; i < 100; i++) {
                buffer.putInt(i);
            }
        }
    }

    public static ByteBuffer serializeTraineeToByteBuffer(Trainee trainee, int bufferSize) throws IOException, UnsupportedEncodingException {

        byte[] byteArr;

        try (ByteArrayOutputStream byteStream = new ByteArrayOutputStream(bufferSize);
             ObjectOutputStream objectStream = new ObjectOutputStream(byteStream)) {

            objectStream.writeObject(trainee);

            byteArr = byteStream.toByteArray();
        }

        return ByteBuffer.wrap(byteArr);
    }
}
