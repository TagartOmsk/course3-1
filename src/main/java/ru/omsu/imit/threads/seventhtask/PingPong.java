package ru.omsu.imit.threads.seventhtask;

import java.util.concurrent.Semaphore;

public class PingPong {
    public static void main(String[] args) {
        Semaphore sem = new Semaphore(1);
        Semaphore sem1 = new Semaphore(0);
        new Thread(new Ping(sem, sem1)).start();
        new Thread(new Pong(sem, sem1)).start();
    }
}
