package ru.omsu.imit.threads.thirteenthtask;

public class Main {
    public static void main(String[] args) {
        Formatter formatter = new Formatter();

        new Thread(new FormatterApp(formatter)).start();
        new Thread(new FormatterApp(formatter)).start();
        new Thread(new FormatterApp(formatter)).start();
        new Thread(new FormatterApp(formatter)).start();
        new Thread(new FormatterApp(formatter)).start();
    }
}
