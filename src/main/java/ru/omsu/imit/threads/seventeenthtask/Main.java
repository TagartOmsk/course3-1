package ru.omsu.imit.threads.seventeenthtask;

import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws ParseException{
        Options options = new Options();

        Option devs = new Option("d", "dev", true, "Developers");
        Option exes = new Option("e", "exe", true, "Executers");

        options.addOption(devs);
        options.addOption(exes);

        CommandLineParser cmdLineParser = new DefaultParser();
        CommandLine cmd = cmdLineParser.parse(options, args);

        int countDevelopers = 2;
        int countExecuters = 5;

        if (cmd.hasOption("d")) {
            countDevelopers = Integer.parseInt(cmd.getOptionValue("d"));
        }

        if (cmd.hasOption("e")) {
            countExecuters = Integer.parseInt(cmd.getOptionValue("e"));
        }


        TaskQueue queue = new TaskQueue();

        List<Thread> developers = new ArrayList<>();

        for (int i = 0; i < countExecuters; i++)
            new Thread(new Executor(queue)).start();

        for (int i = 0; i < countDevelopers; i++) {
            developers.add(new Thread(new Developer(queue, "Developer " + i, 10)));
            developers.get(i).start();
        }

        Thread observer = new Observer(queue);
        observer.start();

        for (int i = 0; i < countDevelopers; i++) {
            try {
                developers.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Thread killer = new Killer(queue);
        killer.start();

        try {
            killer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        observer.interrupt();
    }
}
