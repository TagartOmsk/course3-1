package ru.omsu.imit.threads.seventeenthtask;

public class Killer extends Thread{
    private TaskQueue queue;

    public Killer(TaskQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            queue.getSignal().take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        queue.addTask(new Poison());
    }
}
