package ru.omsu.imit.threads.seventeenthtask;

import ru.omsu.imit.threads.sixteenthtask.Executable;

public class Printer implements Executable {
    private String s;

    public Printer(String s) {
        this.s = s;
    }

    @Override
    public void execute() {
        System.out.println(s);
    }
}
