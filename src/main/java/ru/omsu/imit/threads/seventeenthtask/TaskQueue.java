package ru.omsu.imit.threads.seventeenthtask;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskQueue {
    private final BlockingQueue<Task> queue;
    private final BlockingQueue<Integer> signal;

    private AtomicInteger stagesLeft;

    public TaskQueue() {
        queue = new LinkedBlockingQueue<>();
        stagesLeft = new AtomicInteger(0);
        signal = new LinkedBlockingQueue<>();
    }

    public void addTask(Task task) {
        System.out.println("SIZE: " + stagesLeft.addAndGet(task.getStages().size()));
        queue.offer(task);
    }

    public boolean execute() {
        Task task = null;

        try {
            task = queue.take();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (task instanceof Poison) {
            queue.offer(task);
            return false;
        }
        if (task.isEnd()) {
            return true;
        }

        task.get(task.getCurrentStage()).execute();
        task.next();

        if (stagesLeft.decrementAndGet() == 0) {
            signal.offer(0);
        }

        queue.offer(task);
        return true;
    }

    public BlockingQueue<Integer> getSignal() {
        return signal;
    }

    public Task peek() {
        return queue.peek();
    }

}
