package ru.omsu.imit.threads.seventeenthtask;

public class Executor extends Thread {
    private TaskQueue queue;

    public Executor(TaskQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        boolean check = true;
        while (check) {
            check = queue.execute();
        }
    }
}
