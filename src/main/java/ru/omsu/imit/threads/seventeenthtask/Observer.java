package ru.omsu.imit.threads.seventeenthtask;

public class Observer extends Thread{
    private TaskQueue queue;

    public Observer(TaskQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                return;
            }
            Task current = queue.peek();
            if (current == null)
                System.out.println("NO TASKS");
            else {
                System.out.println("NAME: " + current + " STAGE: " + current.getCurrentStage() + " COUNT: " + current.getStages().size());
            }
        }
    }
}
