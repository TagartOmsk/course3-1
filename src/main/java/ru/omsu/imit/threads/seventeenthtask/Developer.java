package ru.omsu.imit.threads.seventeenthtask;

import ru.omsu.imit.threads.sixteenthtask.Executable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Developer extends Thread{
    private TaskQueue qt;
    private Random random;
    private int count;

    public Developer(TaskQueue qt, String name, int count) {
        this.qt = qt;
        random = new Random();
        this.count = count;
        setName(name);
    }

    @Override
    public void run() {
        for (int i = 0;i < count;i++) {
            List<Executable> exe = new ArrayList<>();
            int size = random.nextInt(5) + 1;
            int t = 0;
            for (int j = 0;j < size;j++) {
                exe.add(new Printer(this.getName() + " " + t + " stage"));
                t++;
            }
            qt.addTask(new Task(exe));
        }
    }
}
