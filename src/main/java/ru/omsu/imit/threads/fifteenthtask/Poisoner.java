package ru.omsu.imit.threads.fifteenthtask;

public class Poisoner implements Runnable{
    private DataQueue queue;

    public Poisoner(DataQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            if(queue.isEmpty()){
                queue.put(new Poison());
                break;
            }
        }
    }
}
