package ru.omsu.imit.threads.fifteenthtask;

public class Data {
    private int[] data;

    public Data(int[] data) {
        this.data = data;
    }

    public int[] get(){
        return data;
    }
}
