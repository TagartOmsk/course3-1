package ru.omsu.imit.threads.fifteenthtask;

import java.util.Arrays;

public class Reader implements Runnable {
    private DataQueue queue;

    public Reader(DataQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            if (!queue.isEmpty()) {
                try {
                    Data data = queue.get();
                    if (data instanceof Poison) {
                        break;
                    }
                    System.out.println(Thread.currentThread().getName() + " read: " + Arrays.toString(data.get()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
