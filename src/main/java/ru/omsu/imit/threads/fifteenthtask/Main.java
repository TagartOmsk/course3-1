package ru.omsu.imit.threads.fifteenthtask;

import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String args[]) throws ParseException {
        Options options = new Options();

        Option writers = new Option("w", "write", true, "Writers");
        Option readers = new Option("r", "read", true, "Readers");

        options.addOption(writers);
        options.addOption(readers);

        CommandLineParser cmdLineParser = new DefaultParser();
        CommandLine cmd = cmdLineParser.parse(options, args);

        int writersCount = 5;
        int countReaders = 5;

        if (cmd.hasOption("w")) {
            writersCount = Integer.parseInt(cmd.getOptionValue("w"));
        }

        if (cmd.hasOption("r")) {
            countReaders = Integer.parseInt(cmd.getOptionValue("e"));
        }

        DataQueue queue = new DataQueue();
        List<Thread> writersList = new ArrayList<>();

        for (int i = 0; i < writersCount; i++) {
            Thread writer = new Thread(new Writer(queue));
            writersList.add(writer);
            writer.start();
        }

        List<Thread> readersList = new ArrayList<>();

        for (int i = 0; i < countReaders; i++) {
            Thread readerThr = new Thread (new Reader(queue));
            readersList.add(readerThr);
            readerThr.start();
        }

        for (Thread writer: writersList) {
            try {
                writer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for(int i = 0; i < readersList.size(); i++){
            queue.put(new Poison());
        }

        for (int i = 0; i < readersList.size(); i++) {
            try {
                readersList.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
