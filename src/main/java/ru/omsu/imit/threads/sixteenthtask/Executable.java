package ru.omsu.imit.threads.sixteenthtask;

public interface Executable {
    void execute();
}
