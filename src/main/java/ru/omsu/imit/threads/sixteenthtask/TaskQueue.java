package ru.omsu.imit.threads.sixteenthtask;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TaskQueue {
    private BlockingQueue<Executable> tasks;

    public TaskQueue() {
        tasks = new LinkedBlockingQueue<>();
    }

    public void put(Executable e) {
        tasks.offer(e);
    }

    public Executable get() throws InterruptedException {
        return tasks.take();
    }

    public boolean isEmpty() {
        return tasks.isEmpty();
    }

}
