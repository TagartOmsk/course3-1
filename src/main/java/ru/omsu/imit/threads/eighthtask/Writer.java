package ru.omsu.imit.threads.eighthtask;

import java.util.Random;

public class Writer implements Runnable {

    private Book book;
    private Random rand = new Random();

    public Writer(Book book) {
        this.book = book;
    }

    @Override
    public void run() {
        for (int i = 0; i < 15; i++) {
            book.write(Integer.toString(rand.nextInt()));
        }
    }
}
