package ru.omsu.imit.threads.eighthtask;

public class ReadWriteSystem {

    public static void main(String[] args) {
        Book book = new Book();
        new Thread(new Writer(book)).start();
        new Thread(new Reader(book)).start();
    }
}
