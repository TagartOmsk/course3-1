package ru.omsu.imit.threads.eighthtask;

public class Reader implements Runnable {

    private Book book;

    public Reader(Book book) {
        this.book = book;
    }

    @Override
    public void run() {
        for(int i = 0; i < 15; i++) {
            System.out.println(book.read());
        }
    }
}
