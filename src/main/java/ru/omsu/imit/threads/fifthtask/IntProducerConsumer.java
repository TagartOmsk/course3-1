package ru.omsu.imit.threads.fifthtask;

import java.util.List;
import java.util.Random;

public class IntProducerConsumer implements Runnable {
    private List<Integer> list;
    private boolean produce;
    private static final int N = 10000;

    public IntProducerConsumer(List<Integer> list, boolean produce) {
        this.list = list;
        this.produce = produce;
        list.clear();
        // Thread thread = new Thread(this);
    }

    @Override
    public synchronized void run() {
        if (produce) {
            fill();
            produce = false;
        } else {
            clear();
        }
    }

    private synchronized void fill() {
        Random rand = new Random();
        for (int i = 0; i < N; i++) {
            list.add(rand.nextInt());
            System.out.println("added " + i);
        }
    }

    private synchronized void clear() {
        Random rand = new Random();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.remove(rand.nextInt(list.size())));
        }
    }
}
