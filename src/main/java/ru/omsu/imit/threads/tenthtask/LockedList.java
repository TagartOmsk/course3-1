package ru.omsu.imit.threads.tenthtask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class LockedList {
    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>();
        ReentrantLock lock = new ReentrantLock();
        new Thread (new IntLockedProducer(integerList, lock)).start();
        new Thread (new IntLockedConsumer(integerList, lock)).start();
    }
}
