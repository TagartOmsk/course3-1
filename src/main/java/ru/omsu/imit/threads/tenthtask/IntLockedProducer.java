package ru.omsu.imit.threads.tenthtask;

import ru.omsu.imit.threads.fourthtask.IntProducer;

import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class IntLockedProducer implements Runnable{
    private List<Integer> integerList;
    private ReentrantLock lock;

    public IntLockedProducer(List<Integer> integerList, ReentrantLock lock) {
        this.integerList = integerList;
        this.lock = lock;
    }

    @Override
    public void run() {
        lock.lock();
        try {
            Random random = new Random();
            for (int i = 0; i < IntProducer.N; i++) {
                integerList.add(random.nextInt());
            }
        } finally {
            lock.unlock();
        }
    }
}
