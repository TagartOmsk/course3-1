package ru.omsu.imit.threads.fourteenthtask;

import java.io.IOException;

public class Sender implements Runnable {
    private Transport transport;

    public Sender(Transport transport) {
        this.transport = transport;
    }

    @Override
    public void run() {
        while (transport.hasMessage()) {
            try {
                transport.send(transport.next());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
