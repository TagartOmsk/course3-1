package ru.omsu.imit.threads.fourteenthtask;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Transport transport = new Transport("src/main/files/incoming", "src/main/files/log",
                " your new hash: taegfdarWERWdag","arara", "2ba3-nyannyan-@xxxxx.com");

       new Thread(new Sender(transport)).start();
       new Thread(new Sender(transport)).start();
       new Thread(new Sender(transport)).start();
       new Thread(new Sender(transport)).start();
       new Thread(new Sender(transport)).start();
       new Thread(new Sender(transport)).start();

        transport.close();
    }
}
