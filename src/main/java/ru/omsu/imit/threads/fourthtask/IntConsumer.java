package ru.omsu.imit.threads.fourthtask;

import java.util.List;
import java.util.Random;

public class IntConsumer implements Runnable {

    private List<Integer> list;
    private Thread thread;

    public IntConsumer(List<Integer> list) {
        this.list = list;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        Random rand = new Random();
        int i = 0;
        while(true) {
            synchronized (list) {
                if (list.size() > 0) {
                    System.out.println(list.remove(rand.nextInt(list.size())));
                    i++;
                }
                if (i == IntProducer.N) break;
            }
        }
    }
}
