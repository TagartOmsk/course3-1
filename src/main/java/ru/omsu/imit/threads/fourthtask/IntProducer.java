package ru.omsu.imit.threads.fourthtask;

import java.util.List;
import java.util.Random;

public class IntProducer implements Runnable {
    private List<Integer> list;
    private Thread thread;
    public static final int N = 10000;

    public IntProducer(List<Integer> list) {
        this.list = list;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        Random rand = new Random();
        for (int i = 0; i < N; i++) {
            synchronized (list) {
                list.add(rand.nextInt());
                System.out.printf("added%d\n", i);
            }
        }
    }
}
