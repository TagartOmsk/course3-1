package ru.omsu.imit.colors;

public interface Colored {

    void setColor(Color color) throws ColorException;

    void setColor(String colorString) throws ColorException;

    Color getColor();

}
