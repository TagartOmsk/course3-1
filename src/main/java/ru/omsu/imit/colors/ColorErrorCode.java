package ru.omsu.imit.colors;

public enum ColorErrorCode {
    WRONG_COLOR_STRING("Incorrect color value, choose another"),
    NULL_COLOR("You set empty pointer instead of actual color");
    private String errorString;

    ColorErrorCode(String errorString){
        this.errorString = errorString;
    }

    public String getErrorString(){
        return errorString;
    }
}
