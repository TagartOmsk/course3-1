package ru.omsu.imit.colors;

public class ColorException extends Exception {
    private ColorErrorCode code;

    public ColorException(ColorErrorCode code){
        super(code.getErrorString());
        this.code = code;
    }

    public ColorErrorCode getErrorCode(){
        return code;
    }
}
